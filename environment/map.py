from environment.tiles import Tuile, Fin


class LevelMap:

    def __init__(self, level_data):
        """
        Object representing the game map's data
        :param level_data: table of dict objects representing tiles properties
        :return: Map instance
        """
        self.__cells__ = []
        self.cars_start_points = []
        self.modified_tiles = dict()
        self.end_tile = None
        # Calling the level build method
        self.__build_cells__(level_data)

    def __build_cells__(self, level_data):
        """
        Builds the list of cells objects as per level data provided
        :param level_data: table of dict objects representing tiles properties
        :return: Bool value depending on operation success
        """
        self.end_tile = Fin()
        for y, level_row in enumerate(level_data):
            # For each row of level data and for each cell of a row
            self.__cells__.append([])  # Adding a new row to the cells
            for x, tile_props in enumerate(level_row):
                # We create a new Tile object and append it to the current line
                new_tile_object = Tuile(tile_props, x, y, self.end_tile)
                # Detecting start points
                if "start" in tile_props.keys():
                    self.cars_start_points.append(new_tile_object)
                # Updating tile's neighbors values
                if y >= 1:
                    new_tile_object.update_neighbour("N", self.__cells__[y - 1][x])
                    self.__cells__[y - 1][x].update_neighbour("S", new_tile_object)

                if x >= 1:
                    new_tile_object.update_neighbour("O", self.__cells__[y][x - 1])
                    self.__cells__[y][x - 1].update_neighbour("E", new_tile_object)

                self.__cells__[-1].append(new_tile_object)

    def get_cell_image(self, x, y):
        return self.__cells__[y][x].get_image()

    def get_tile(self, x_pos, y_pos):
        return self.__cells__[y_pos][x_pos]

    def get_height(self):
        return len(self.__cells__)

    def get_width(self):
        return len(self.__cells__[0])

    def remove_actor_from_tile(self, actor, tile):
        tile.occupants.remove(actor)
        self.redraw_tile(tile)

    def add_actor_to_tile(self, actor, tile):
        tile.occupants.append(actor)
        self.redraw_tile(tile)
        return len(tile.occupants)

    def redraw_tile(self, tile):
        self.modified_tiles[tile] = True

    def reset_modified_tiles(self):
        self.modified_tiles = dict()